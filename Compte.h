#ifndef COMPTE_H_INCLUDED
#define COMPTE_H_INCLUDED

#include <iostream>
using namespace std;

class compte
{

public :

compte();
compte(int NumCompte,string NomProprietaire,float Solde);
void DeposerArgent();
void SetNumCompte();
void SetName();
bool TransferArgent(compte &AutreCompte);
void ConsulterSolde();
virtual bool RetirerArgent(float montant);
virtual void ConsulterUtilisateur();
int getNumcompte();

protected :

int NumCompte;
string NomProprietaire;
float Solde;
};

#endif // COMPTE_H_INCLUDED
