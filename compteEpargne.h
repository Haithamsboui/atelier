#ifndef COMPTEEPARGNE_H
#define COMPTEEPARGNE_H

#include "comptes.h"

class CompteJeune : public CompteEpargne
{
public :
    CompteJeune();
    CompteJeune(int c_NumCompte,string c_NomProprietaire,float c_Solde,int intersetPercentage);
    void ConsulterUtilisateur();
    void setIntersetPercentage();
    void SetInterest();
private :
    int intersetPercentage;
};

class CompteMaison : public CompteEpargne
{

public :
    CompteMaison();
    CompteMaison(int c_NumCompte,string c_NomProprietaire,float c_Solde,int intersetPercentage);
    void ConsulterUtilisateur();
    void setIntersetPercentage();
    void SetInterest();
private :
    int intersetPercentage;
};


#endif // COMPTEEPARGNE_H
