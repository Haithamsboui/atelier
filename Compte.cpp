#include "Compte.h"

compte::compte():NumCompte(0),NomProprietaire(""),Solde(0)
{
}

compte::compte(int c_NumCompte,string c_NomProprietaire,float c_Solde):NumCompte(c_NumCompte),NomProprietaire(c_NomProprietaire),Solde(c_Solde)
{
}

void compte::ConsulterSolde()
{
    cout << "Your Balance : "<<Solde<<endl;
}

void compte::DeposerArgent()
{
    float montant;
    do
    {
        cout<<"Amount : ";
        cin >> montant;
        if(montant<0)
            cout<<"invalid amount !"<<endl;
    }
    while(montant<0);
    Solde+=montant;
}

bool compte::TransferArgent(compte &AutreCompte)
{
    float montant;
    cout <<"Amount to transfer : ";
    cin >>montant;
    if ( montant>Solde )

        return false;

    else
    {
        AutreCompte.Solde+=montant;
        Solde-=montant;
        return true;
    }

}

bool compte::RetirerArgent(float montant)
{
    if(montant>Solde)
        return false;
    else
    {

        Solde-=montant;
        return true;
    }
}

void compte::SetNumCompte()
{
    cout << "Account number : ";
    do
    cin >>NumCompte;
    while (NumCompte<0);
}

void compte::SetName()
{
    cout << "Name : ";
    cin >>NomProprietaire;
}

void compte::ConsulterUtilisateur()
{
    cout <<"Name : "<<NomProprietaire<<"\t Account number : "<<NumCompte<<"\t Balance : "<<Solde<<"\t Type : Normal"<<endl;
}

int compte::getNumcompte()
{
    return NumCompte;
}
