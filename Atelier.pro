TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += \
    BankMED.cpp \
    main.cpp \
    Compte.cpp \
    comptes.cpp \
    compteEpargne.cpp

HEADERS += \
    BankMED.h \
    Compte.h \
    comptes.h \
    compteEpargne.h

OTHER_FILES +=

