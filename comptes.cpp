#include "comptes.h"

CompteEpargne::CompteEpargne():compte(0,"",20)
{
}

CompteEpargne::CompteEpargne(int c_NumCompte,string c_NomProprietaire,float c_Solde)
{
    if(c_Solde<20)
        compte(c_NumCompte,c_NomProprietaire,20);
    else
        compte(c_NumCompte,c_NomProprietaire,c_Solde);

}

bool CompteEpargne::RetirerArgent(float montant)
{
    if(montant+20>Solde)
        return false;
    else
    {
        Solde-=montant;
        return true;
    }
}



CompteCourant::CompteCourant():compte(0,"",5)
{
}

CompteCourant::CompteCourant(int c_NumCompte,string c_NomProprietaire,float c_Solde)
{
    if(c_Solde<5)
        compte(c_NumCompte,c_NomProprietaire,5);
    else
                compte(c_NumCompte,c_NomProprietaire,c_Solde);

}

bool CompteCourant::RetirerArgent(float montant)
{
    if(montant+5>Solde)
        return false;
    else
    {
        Solde-=montant;
        return true;
    }
}

void CompteCourant::ConsulterUtilisateur()
{
    cout <<"Name : "<<NomProprietaire<<"\t Account number : "<<NumCompte<<"\t Balance : "<<Solde<<"\t Type : Transactional"<<endl;

}
