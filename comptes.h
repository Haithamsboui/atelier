#ifndef COMPTES_H
#define COMPTES_H

#include "Compte.h"


class CompteEpargne : public compte
{
public:
    CompteEpargne();
    CompteEpargne(int c_NumCompte,string c_NomProprietaire,float c_Solde);
    bool RetirerArgent(float montant);
virtual void ConsulterUtilisateur()=0;

};


class CompteCourant : public compte
{
public:
    CompteCourant();
    CompteCourant(int c_NumCompte,string c_NomProprietaire,float c_Solde);
    bool RetirerArgent(float montant);
    void ConsulterUtilisateur();


};


#endif // COMPTES_H
